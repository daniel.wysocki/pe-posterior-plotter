"""Bayesian parametric population models (pop_models)

Long description goes here...
"""

from datetime import date


#-------------------------------------------------------------------------------
#   GENERAL
#-------------------------------------------------------------------------------
__name__        = "ligo_pe_plotter"
__version__     = "0.1.0a1"
__date__        = date(2018, 1, 1)
__keywords__    = [
    "astronomy",
    "Bayesian inference",
    "gravitational waves",
    "physics",
    "visualization",
]
__status__      = "Alpha"


#-------------------------------------------------------------------------------
#   URLS
#-------------------------------------------------------------------------------
__url__         = "https://git.ligo.org/daniel.wysocki/pe-posterior-plotter"
__bugtrack_url__= "https://git.ligo.org/daniel.wysocki/pe-posterior-plotter/issues"


#-------------------------------------------------------------------------------
#   PEOPLE
#-------------------------------------------------------------------------------
__author__      = "Daniel Wysocki and Richard O'Shaughnessy"
__author_email__= "daniel.wysocki@ligo.org"

__maintainer__      = "Daniel Wysocki"
__maintainer_email__= "daniel.wysocki@ligo.org"

__credits__     = ("Daniel Wysocki", "Richard O'Shaughnessy",)


#-------------------------------------------------------------------------------
#   LEGAL
#-------------------------------------------------------------------------------
__copyright__   = 'Copyright (c) 2018 {author} <{email}>'.format(
    author=__author__,
    email=__author_email__
)

__license__     = 'MIT'
__license_full__= '''
Licensed under the MIT license
https://opensource.org/licenses/MIT
'''.strip()


#-------------------------------------------------------------------------------
#   PACKAGE
#-------------------------------------------------------------------------------
DOCLINES = __doc__.split("\n")

CLASSIFIERS = """
Development Status :: 3 - Alpha
Programming Language :: Python
Programming Language :: Python :: 2
Programming Language :: Python :: 3
Operating System :: OS Independent
Intended Audience :: Science/Research
Topic :: Scientific/Engineering :: Astronomy
Topic :: Scientific/Engineering :: Physics
""".strip()

REQUIREMENTS = {
    "install": [
        "numpy>=1.13.0,<1.14.0",
        "matplotlib>=2.0.0,<3.0.0",
        "scipy>=0.18.0,<0.19.0",
        "seaborn>=0.8.0,<0.9.0",
        "six>=1.10.0,<1.11.0",
    ],
    "tests": [
    ]
}

ENTRYPOINTS = {
    "console_scripts" : [
        "ligo_pe_plotter = ligo_pe_plotter.cli:_main",
    ]
}

from setuptools import find_packages, setup

metadata = dict(
    name        =__name__,
    version     =__version__,
    description =DOCLINES[0],
    long_description='\n'.join(DOCLINES[2:]),
    keywords    =__keywords__,

    author      =__author__,
    author_email=__author_email__,

    maintainer  =__maintainer__,
    maintainer_email=__maintainer_email__,

    url         =__url__,

    license     =__license__,

    classifiers=[f for f in CLASSIFIERS.split('\n') if f],

    package_dir ={"": "src"},
    packages    =[
        "ligo_pe_plotter",
    ],

    install_requires=REQUIREMENTS["install"],
#    tests_require=REQUIREMENTS["tests"],

    entry_points=ENTRYPOINTS
)

setup(**metadata)
