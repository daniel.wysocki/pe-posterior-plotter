r"""
Defines properties of parameters.
"""

def _default_ranges():
    return {
        "m1_source": (0.0, 50.0),
        "m2_source": (0.0, 50.0),
        "q": (0.0, 1.0),
        "chi_eff": (-1.0, +1.0),
    }

def _default_labels():
    return {
        "m1_source": r"$m_1^{\mathrm{source}}$ [M$_\odot$]",
        "m2_source": r"$m_2^{\mathrm{source}}$ [M$_\odot$]",
        "q": r"$q$",
        "chi_eff": r"$\chi_{\mathrm{eff}}$",
    }


class ParamRanges(object):
    def __init__(self):
        self._ranges = _default_ranges()

    def __getitem__(self, param):
        if param in self._ranges:
            return self._ranges[param]
        else:
            return 0.0, 1.0

    def __setitem__(self, param, min_max):
        try:
            p_min, p_max = min_max
        except:
            raise ValueError(
                "Can only assign a pair (min, max) as the value."
            )

        self._ranges[param] = p_min, p_max

    def __contains__(self, param):
        return param in self._ranges


class ParamLabels(object):
    def __init__(self):
        self._labels = _default_labels()

    def __getitem__(self, param):
        if param in self._labels:
            return self._labels[param]
        else:
            return param

    def __setitem__(self, param, label):
        self._labels[param] = label

    def __contains__(self, param):
        return param in self._labels
