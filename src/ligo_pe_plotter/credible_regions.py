r"""
Tools for computing credible regions.
"""
from __future__ import division, print_function


def cr_and_kde(samples, quantiles, weights=None):
    import numpy

    from .utils import gaussian_kde

    samples = numpy.asarray(samples)

    ndim = numpy.ndim(samples)
    assert ndim in {1, 2}, "Only accepts 1- and 2-D arrays."

    shape = numpy.shape(samples)
    n_samples = shape[0] if ndim == 1 else shape[1]

    kde = gaussian_kde(samples, weights=weights)
    prob_samples = kde(samples)

    i_sort = numpy.argsort(prob_samples)
    samples_sorted = (
        samples[i_sort]
        if ndim == 1
        else samples[:, i_sort]
    )

    i_quantiles = numpy.floor((1-quantiles) * n_samples).astype(numpy.int32)

    samples_cr = (
        samples_sorted[i_quantiles]
        if ndim == 1
        else samples_sorted[:, i_quantiles]
    )
    prob_cr = kde(samples_cr)
    prob_cr.sort()

    return prob_cr, kde
