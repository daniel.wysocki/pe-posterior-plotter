"""
Command line interface for LIGO PE Posterior Plotter
"""
from __future__ import division, print_function


def get_args(raw_args):
    import argparse


    parser = argparse.ArgumentParser()

    params_group = parser.add_mutually_exclusive_group(required=True)


    parser.add_argument(
        "output_plot",
        help="Filename for plot to output.",
    )

    params_group.add_argument(
        "--param",
        help="Single parameter to plot posteriors for.",
    )
    params_group.add_argument(
        "--params",
        metavar=("PARAM_X", "PARAM_Y"),
        nargs=2,
        help="Pair of parameters to plot posteriors for.",
    )

    parser.add_argument(
        "--param-range",
        metavar=("PARAM", "PARAM_MIN", "PARAM_MAX"),
        nargs=3,
        action="append",
        default=[],
        help="Specify the range of values allowed for a given parameter. "
             "This is not required -- you do not need to specify the range for "
             "any of the parameters you use, and they will take reasonable "
             "default values in some cases, and [0, 1] in others. You may also "
             "provide ranges for parameters you don't even use -- this can be "
             "useful if you make repeated calls to this script with different "
             "params, and do not want to modify the argument list each time.",
    )

    parser.add_argument(
        "--param-label",
        metavar=("PARAM", "LABEL"),
        nargs=2,
        action="append",
        default=[],
        help="Specify the label used for a given parameter. This is not "
             "required -- you do not need to specify the label for any of the "
             "parameters you use, and they will take reasonable default values "
             "in some cases, and the basic name of the param otherwise. You "
             "may also provide labels for parameters you don't even use -- "
             "this can be useful if you make repeated calls to this script "
             "with different params, and do not want to modify the argument "
             "list each time.",
    )

    parser.add_argument(
        "-e", "--event",
        nargs="+",
        required=True,
        action="append",
        help="Posterior file for event",
    )

    parser.add_argument(
        "--percentiles",
        metavar="P",
        type=float, nargs="+",
        default=[50.0, 90.0],
        help="Percentiles to display on plots, default is 50%% and 90%%.",
    )

    parser.add_argument(
        "--n-samples",
        type=int, default=100,
        help="Number of samples to use for each parameter.",
    )

    parser.add_argument(
        "--max-posteriors",
        type=int,
        help="(Optional) maximum number of posterior samples to use.",
    )

    parser.add_argument(
        "--legend",
        action="store_true",
        help="Display legend.",
    )

    parser.add_argument(
        "--fig-size",
        type=float, nargs=2,
        default=[8, 8],
        help="Dimensions of figure.",
    )

    parser.add_argument(
        "--use-tex",
        action="store_true",
        help="Use TeX backend for text rendering.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib.",
    )


    args = parser.parse_args(raw_args)


    return args


def _main(raw_args=None):
    import sys


    if raw_args is None:
        raw_args = sys.argv[1:]

    args = get_args(raw_args)


    import os

    import numpy
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    matplotlib.rcParams["text.usetex"] = args.use_tex

    from . import plot
    from .params import ParamRanges, ParamLabels

    n_params = 1 if args.params is None else 2

    if n_params == 1:
        params = [args.param]

        raise NotImplementedError("1D plots not yet implemented")
    else:
        params = args.params


    param_ranges = ParamRanges()
    param_labels = ParamLabels()

    for param, param_min, param_max in args.param_range:
        param_ranges[param] = param_min, param_max

    for param, label in args.param_label:
        param_labels[param] = label


    fig, ax = plt.subplots(figsize=args.fig_size)

    for param, label_fn in zip(params, [ax.set_xlabel, ax.set_ylabel]):
        label_fn(param_labels[param])

    grids_1d = []
    for param in params:
        p_min, p_max = param_ranges[param]
        grids_1d.append(numpy.linspace(p_min, p_max, args.n_samples))
    grid = numpy.meshgrid(*grids_1d)

    if n_params == 1:
        grid = grid[0]


    quantiles = numpy.divide(args.percentiles, 100.0)

    for event in args.event:
        fname = event[0]
        options = dict(s.split("=", 1) for s in event[1:])

        # Label defaults to filename without path and extension
        if "label" not in options:
            options["label"] = os.path.splitext(os.path.basename(fname))[0]

        samples_full = numpy.genfromtxt(fname, names=True)
        available_params = samples_full.dtype.names

        samples = []
        for param in params:
            if param not in available_params:
                raise ValueError(
                    "File '{fname}' missing parameter '{param}'"
                    .format(fname=fname, param=param)
                )

            param_samples = samples_full[param]
            if args.max_posteriors is not None:
                param_samples = param_samples[:args.max_posteriors]

            samples.append(param_samples)

        del available_params, samples_full

        plot.plot(
            ax, samples, quantiles, grid,
            **options
        )

    # Cover regions with m2 > m1
    if params == ["m1_source", "m2_source"]:
        m1_min, m1_max = ax.get_xlim()
        m2_min, m2_max = ax.get_ylim()

        m_min = min(m1_min, m2_min)

        ax.fill_between(
            [m_min, m1_max],
            [m_min, m1_max], [m2_max, m2_max],
            color="#444444", zorder=numpy.inf,
        )
    if params == ["m2_source", "m1_source"]:
        m1_min, m1_max = ax.get_ylim()
        m2_min, m2_max = ax.get_xlim()

        m_min = min(m1_min, m2_min)

        ax.fill_between(
            [m_min, m2_max],
            [m_min, m2_max], [m_min, m_min],
            color="#444444", zorder=numpy.inf,
        )

    if args.legend:
        handles, labels = ax.get_legend_handles_labels()

        if len(handles) > 0:
            leg = ax.legend(handles, labels, loc="best")
            leg.set_zorder(numpy.inf)

    fig.savefig(args.output_plot)
