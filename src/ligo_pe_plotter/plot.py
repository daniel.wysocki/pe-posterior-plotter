r"""
Plotting tools.
"""
from __future__ import division, print_function


def plot(
        ax, samples, quantiles, grid,
        weights=None,
        color="black", linestyle="-", linewidth=2,
        label=None,
    ):
    import numpy

    ndim = numpy.ndim(samples)
    assert ndim in {1, 2}, "Only accepts 1- and 2-D arrays."

    if ndim == 1:
        return plot1d(
            ax, samples, quantiles, grid,
            weights=weights,
            color=color, linestyle=linestyle, linewidth=linewidth,
            label=label,
        )
    elif ndim == 2:
        return plot2d(
            ax, samples, quantiles, grid,
            weights=weights,
            color=color, linestyle=linestyle, linewidth=linewidth,
            label=label,
        )


def plot1d(
        ax, samples, quantiles, grid,
        weights=None,
        color="black", linestyle="-", linewidth=2,
        label=None,
    ):
    raise NotImplementedError


def plot2d(
        ax, samples, quantiles, grid,
        weights=None,
        color="black", linestyle="-", linewidth=2,
        label=None,
    ):
    from . import credible_regions

    gridx, gridy = grid

    prob_cr, kde = credible_regions.cr_and_kde(
        samples, quantiles,
        weights=weights,
    )
    prob = kde((gridx.ravel(), gridy.ravel())).reshape(gridx.shape)

    ctr = ax.contour(
        gridx, gridy, prob,
        prob_cr,
        colors=color, linestyles=linestyle, linewidths=linewidth,
    )

    if label is not None:
        ctr.collections[0].set_label(label)

    return ax
